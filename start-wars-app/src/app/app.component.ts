import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';
import * as firebase from 'firebase';
import { HelpersService } from './services/helpers.service';
import { NavigationHistoryService } from './services/navigation-history.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private api: ApiService, 
              private navigationHistory: NavigationHistoryService,
              private helpers: HelpersService) {
  }

  ngOnInit() {
       // Initialize Firebase
       const config = {
        apiKey: "AIzaSyCxEZrT9u_sAu79lodpPIU2hMoxN69maAQ",
        authDomain: "opinnion-85d28.firebaseapp.com",
        databaseURL: "https://opinnion-85d28.firebaseio.com",
        projectId: "opinnion-85d28",
        storageBucket: "opinnion-85d28.appspot.com",
        messagingSenderId: "589244991008"
      };
      firebase.initializeApp(config);
      
      //Si estamos autenticados redirigimos a la pantalla principal
      if(this.api.isAuthenticated()) {
        this.helpers.navigateTo('/main');
      }   

      //Nos subscribimos a los cambios de routa para guardar el historial de navegación
      this.navigationHistory.subscribeToNav();
  }

}
