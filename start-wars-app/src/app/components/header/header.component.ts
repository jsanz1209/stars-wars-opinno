import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { HelpersService } from '../../services/helpers.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  
  subscription: Subscription;
  isShowLoading = null;
  constructor(private helpers: HelpersService, private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  //Función que reedirige a la pantalla princiapl
  goToMain() {
    this.helpers.navigateTo('/main');
  }

  //Función que reedirige a la pantalla del historial de navegación
  goToHistory() {
    this.helpers.navigateTo('/main/history-navigation');
  }

  //Función que realiza un logout y redirige a la pantalla de login
  goToLogout() {
    var self = this;
    this.isShowLoading = true;
    this.subscription = self.api.logout().subscribe(() => {
      this.isShowLoading = false;
      self.helpers.navigateTo('/login');
    }, error => {
      this.isShowLoading = false;
      console.log(error);
    }); 
  }

  ngOnDestroy() {
    //Eliminamos las subscripciones
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }


}
