import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnDestroy {
  public _showMe: boolean;

  @Input()
  public set showMe(value: boolean) {
    //Si mostramos el loading añadimos la clase que evita los eventos de click
    if(value){
      document.body.classList.add('loading-show');
    //En caso contrario la borramos
    }else{
      document.body.classList.remove('loading-show');
    }
    this._showMe = value;
  }


  ngOnDestroy(){
    //Al destruir el componente borrar la clase que evita los eventos de click
    document.body.classList.remove('loading-show');
  }

}
