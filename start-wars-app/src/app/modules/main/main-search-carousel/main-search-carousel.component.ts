import { Component, OnInit, ChangeDetectorRef, AfterViewChecked, OnDestroy } from '@angular/core';
import { NguCarouselConfig} from '@ngu/carousel';
import { zip, Subscription } from 'rxjs';
import { ApiService } from '../../../services/api.service';
import { CacheService } from '../../../services/cache.service';
import { HelpersService } from '../../../services/helpers.service';


@Component({
  selector: 'app-main-search-carousel',
  templateUrl: './main-search-carousel.component.html',
  styleUrls: ['./main-search-carousel.component.scss']
})
export class MainSearchCarosuelComponent implements OnInit, OnDestroy, AfterViewChecked {

  //Elementos del carousel
  public carouselTileItems: Array<any> = [];
  //Configuración del carousel
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 2, md: 3, lg: 4, all: 0 },
    slide: 3,
    speed: 900,
    load: 3,
    velocity: 0,
    touch: false,
    loop:true,   
    point: {
      visible: true,
      hideOnSingleSlide: true
    },
    easing: 'ease-in-out'
  };

  //Películas cargadas
  films = [];

  //Subscripción de la api
  subscriptionCallApi: Subscription;

  //Indica si debe mostrar o no el loading
  isShowLoading = false;

  
  constructor(private api: ApiService, 
      private cache: CacheService, 
      private helpers: HelpersService,
      private cdRef:ChangeDetectorRef) { }

  ngOnInit() {

    //Si tenemos cacheadas las películas y los personajes, los recuperamos
    if(this.cache.cacheFilms !== null && this.cache.cachePeople.length !== null) {
      this.films = this.cache.cacheFilms;
      this.carouselTileItems = this.cache.cachePeople;
    //En caso contario, hacemos la llamada los REST
    } else {
      this.isShowLoading = true;      
      //Pila de llamadas
      const callStack = zip(
        this.api.getFilms(),        
        this.api.getPeople()
      );
      this.subscriptionCallApi = callStack.subscribe(res => {
        this.isShowLoading = false;
        //Si la información de los personajes se recupera correctamente
        if(res[0] && res[0] !== null) {
          this.cache.cacheFilms = res[0].results;
          this.films = [ ... res[0].results];
        }
  
        //Si la información de los personajes se recupera correctamente
        if(res[1] && res[1] !== null) {
          this.carouselTileItems = res[1].results;
          this.cache.cachePeople = res[1].results;
        }
      },
      error => {
        this.isShowLoading = false;
        alert(error);
      });  

    }
    
  }

  ngAfterViewChecked(){
    this.cdRef.detectChanges();
  }

  //Reedirige a la pantalla de información de un personaje seleccionado
  goToDetailPerson(id) {
    this.helpers.navigateTo('/main/person-detail/' + id);
  }

  ngOnDestroy() {
    //Eliminamos las subscripciones existentes
    if(this.subscriptionCallApi) {
      this.subscriptionCallApi.unsubscribe();
    }
  }
}
