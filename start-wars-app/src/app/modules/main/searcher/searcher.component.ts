import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.scss']
})
export class SearcherComponent implements OnInit {

  //Entradas de las películas recuperadas de la api
  @Input('films') films;
  //Variable que indica si se ha producido un blur del input text
  onblurInput = false;
  //Cadena para filtrar las distintas películas
  filterSearch = '';

  constructor() { }

  ngOnInit() {
  }

  //Convierte una cadena en fecha
  getDate(releaseDate) {
    return new Date(releaseDate);
  }

}
