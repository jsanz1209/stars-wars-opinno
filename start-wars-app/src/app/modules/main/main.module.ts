import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { SearcherComponent } from './searcher/searcher.component';
import { SharedModule } from '../shared/shared.module';
import { NguCarouselModule } from '@ngu/carousel';
import { FormsModule } from '@angular/forms';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { MainSearchCarosuelComponent } from './main-search-carousel/main-search-carousel.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import { HistoryNavigationComponent } from './history-navigation/history-navigation.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NguCarouselModule,    
    FormsModule,
    MainRoutingModule
  ],
  declarations: [
    MainScreenComponent, 
    MainSearchCarosuelComponent, 
    SearcherComponent, 
    PersonDetailComponent, 
    HistoryNavigationComponent
  ]
})
export class MainModule { }
