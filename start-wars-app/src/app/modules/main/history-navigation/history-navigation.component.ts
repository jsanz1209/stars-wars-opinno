import { Component, OnInit } from '@angular/core';
import { NavigationHistoryService } from '../../../services/navigation-history.service';

@Component({
  selector: 'app-history-navigation',
  templateUrl: './history-navigation.component.html',
  styleUrls: ['./history-navigation.component.scss']
})
export class HistoryNavigationComponent implements OnInit {

  navigations = [];
  navigationKeys = [];

  constructor(private navigationHistory: NavigationHistoryService) { }

  ngOnInit() {
    this.loadHistory();
  }

  //Cargamos el historial de navegación
  loadHistory() {
    //Historial de navegación
    this.navigations = this.navigationHistory.getHistory();
    //Entrada de fecha del historial de navegación
    this.navigationKeys = this.navigations !== null ? Object.keys(this.navigations) : null;
  }

  //Obtiene la fecha
  getDate(historyDate) {
    const dateSplit = historyDate.split('-');
    const dateObject = new Date();
    dateObject.setDate(dateSplit[0]);
    dateObject.setMonth(dateSplit[1]);
    dateObject.setFullYear(dateSplit[2]);
    return dateObject;
  }

  //Borra todo el historial de navegación
  onDeleteAllHistory() {
    this.navigationHistory.deleteAllHistory();
    this.loadHistory();
  }

  //Borra una entrada del historial de navegación
  onDeleteHistory(date, index) {    
    this.navigationHistory.deleteHistory(date, index);
    this.loadHistory();
  }

}
