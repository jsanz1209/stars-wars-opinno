import { Component, OnInit, OnDestroy, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Subscription, zip } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { NguCarouselConfig } from '@ngu/carousel';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.scss']
})
export class PersonDetailComponent implements OnInit, OnDestroy, AfterViewChecked {

  //Subscripciones
  paramSubscription: Subscription;
  apiSubscripton: Subscription;
  apiZipSubscripton: Subscription;

  //Indica si debe mostrar o no el loading
  isShowLoading = false;

  //Información general del personaje
  mainInfoPerson = null;
  //Especie del personaje
  specieInfo = null;
  //Mundo del personaje
  homeWorldInfo = null;
  //Películas en las que ha participado
  filmsInfo = [];

  //Lista de elementos del carousel
  public carouselTileItems: Array<any> = [];
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
    slide: 3,
    speed: 900,
    load: 3,
    velocity: 0,
    touch: false,
    loop:true,   
    point: {
      visible: true,
      hideOnSingleSlide: true
    },
    easing: 'ease-in-out'
  };


  constructor(private route: ActivatedRoute,
              private api: ApiService,
              private cdRef:ChangeDetectorRef) { }

  ngOnInit() {
    //Escuchamos el id del personaje que queremos consultar
    this.paramSubscription = this.route.params.subscribe(params => {
      const idPerson = params['id'];
      //Obtenemos los detalles del personaje
      this.getDetailPerson(idPerson);
    });
  }

  ngAfterViewChecked(){
    this.cdRef.detectChanges();
  }

  getDetailPerson(id) {
    if(id && id !== null) {
      this.isShowLoading = true;
      //Llamamos a la api de detalle del personaje
      this.apiSubscripton =  this.api.getPerson(id).subscribe(response => {
        this.mainInfoPerson = response;
        //Prepara la pila de llamadas de las películas, especies y home world
        const zipItems = this.prepareZipItems(response);
        if(zipItems.length > 0) {          
          const zip0$ = (a$) => zip(...zipItems[0]);
          const zip1$ = (a$) => zip(...zipItems[1]);
          const zip2$ = (a$) => zip(...zipItems[2]);

          //Pila de los zips de las distintas llamadas
          const callStack = zip(
            zip0$(zipItems[0]),      
            zip1$(zipItems[1]),
            zip2$(zipItems[2])
          );
    
          //Recuperamos las respuestas de la distintas llamadas
          this.apiZipSubscripton = callStack.subscribe(responseZip => {
            this.carouselTileItems = responseZip[0];
            this.specieInfo = responseZip[1][0];
            this.homeWorldInfo = responseZip[2][0];
            this.isShowLoading = false;
          },  
          error => {
            alert(error);
            this.isShowLoading = false;
          });
        }
        
      }, error => {
        this.isShowLoading = false;
        alert(error);
      });
    }
  }

  //Prepara los zips de las películas, especies y homeworld
  prepareZipItems(response) {
    const zipItems = [[], [], []];
    response.films.forEach(element => {
        zipItems[0].push(this.api.requestGeneric(element));
    });

    response.species.forEach(element => {
      zipItems[1].push(this.api.requestGeneric(element));
    });

    if(response.homeworld !== null && response.homeworld !== '') {
      zipItems[2].push(this.api.requestGeneric(response.homeworld));
    }

    return zipItems;
  }

  //Obtiene la fecha a partir de una cadena
  getDate(releaseDate) {
    return new Date(releaseDate);
  }

  ngOnDestroy() {
    //Eliminamos las subscripciones
    if(this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }

    if(this.apiSubscripton) {
      this.apiSubscripton.unsubscribe();
    }

    if(this.apiZipSubscripton) {
      this.apiZipSubscripton.unsubscribe();
    }
  }

}
