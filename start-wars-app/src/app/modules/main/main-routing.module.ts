import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardService } from '../../services/authguard.service';
import { MainSearchCarosuelComponent } from './main-search-carousel/main-search-carousel.component';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import { HistoryNavigationComponent } from './history-navigation/history-navigation.component';


const mainRoutes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },


  {
    path: 'main',
    component: MainScreenComponent,
    canActivateChild: [AuthguardService],
    children: [
      {
        path: '',
        component: MainSearchCarosuelComponent
      },
      {
        path:"history-navigation",
        component: HistoryNavigationComponent
      },
      {
        path:"person-detail/:id",
        component: PersonDetailComponent
      }
    ]
  }
  

];


@NgModule({
  imports: [
    RouterModule.forChild(mainRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MainRoutingModule { }
