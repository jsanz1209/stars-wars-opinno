import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelpersService } from '../../services/helpers.service';
import { ApiService } from '../../services/api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  signInForm: FormGroup;
  subscriptionApi: Subscription;
  isShowLoading = false;

  constructor(private fb: FormBuilder, 
              private helpers: HelpersService, 
              private api: ApiService) {
    //Creamos el formulario de login
    this.createFormSignIn();
  }

  ngOnInit() {
  }

  //Creación del formulario de login
  createFormSignIn() {
    this.signInForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      passaccess: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  //Evento que se produce cuando submiteamos el formulario
  onSubmitSignInForm() {
    //Actualizamos el estado de los distintos campos del formulario, para que muestre los mensajes de error cuando submiteamos directamente el formulario
    this.helpers.updateFormStatus(this.signInForm);
    //Si el formulario es correcto
    if(this.signInForm.valid) {
      const username = this.signInForm.controls.username.value;
      const passaccess = this.signInForm.controls.passaccess.value;
      this.isShowLoading = true;
      //Hacemos una llamada a la api de login
      this.subscriptionApi = this.api.signinIn(username, passaccess).subscribe(Response => {
        this.isShowLoading = false;
        this.helpers.navigateTo('/main');
      }, error => {
        this.isShowLoading = false;
        alert(error);
      });
    }
  }

  //Vamos a la pantalla de registro
  goToRegistro() {
    this.helpers.navigateTo('/register');
  }

  ngOnDestroy() {
    //Eliminamos las subscripciones
    if(this.subscriptionApi) {
      this.subscriptionApi.unsubscribe();
    }
  }

}
