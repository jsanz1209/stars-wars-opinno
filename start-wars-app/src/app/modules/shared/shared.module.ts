import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoadingComponent } from '../../components/loading/loading.component';
import { HeaderComponent } from '../../components/header/header.component';
import { SearchPipe } from '../../pipes/search.pipe';
import { SortPipe } from '../../pipes/sort.pipe';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    LoadingComponent,
    HeaderComponent,
    SearchPipe,
    SortPipe
  ],
  exports: [
    SearchPipe,
    SortPipe,
    LoadingComponent,
    HeaderComponent
  ]
})
export class SharedModule { }
