import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HelpersService } from '../../services/helpers.service';
import { ApiService } from '../../services/api.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  signUpForm: FormGroup;
  subscriptionApi: Subscription;
  isShowLoading = false;

  constructor(private fb: FormBuilder, 
              private helpers: HelpersService, 
              private api: ApiService) { 
    //Creamos el formulario de registro
    this.createFormSignUp();
  }

  ngOnInit() {
  }


  //Creamos el formulario de registro
  createFormSignUp() {
    this.signUpForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      newpassword: ['', [Validators.required, Validators.minLength(6)]],
      verifynewpass: ['', [Validators.required, Validators.minLength(6)]]

    });
  }

  //Evento que se produce cuando submiteamos el formulario
  onSubmitSignUpForm() {
    //Actualizamos el estado de los distintos campos del formulario, para que muestre los mensajes de error cuando submiteamos directamente el formulario
    this.helpers.updateFormStatus(this.signUpForm);
    //Recuperamos el valor de los distintos campos del formulario
    const username = this.signUpForm.controls.username.value;
    const newpassword = this.signUpForm.controls.newpassword.value;
    const verifynewpass = this.signUpForm.controls.verifynewpass.value;
    //Si es correcto y las contraseñas coinciden
    if(this.signUpForm.valid && newpassword == verifynewpass) {
      this.isShowLoading = true;
      //Llamos a la api de registro
      this.api.signinUp(username, newpassword).subscribe(response => {
        this.helpers.navigateTo('/login');
        this.isShowLoading = false;
      }, error => {
          this.isShowLoading = false;
          alert(error);
      });
    }

  }

  //Reedirige a la pantalla de login
  goToLogin() {
    this.helpers.navigateTo('/login');
  }

  ngOnDestroy() {
    //Eliminamos las subscripciones
    if(this.subscriptionApi) {
      this.subscriptionApi.unsubscribe();
    }
  }


}
