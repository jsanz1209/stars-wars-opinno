import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: "sort"
})
export class SortPipe {
  transform(array: Array<string>, type?: string): Array<string> {
    //Si hemos incidacado 'reverse' ordenamos el array en orden inverson
    if(type && type== 'reverse') {
      array.reverse();
    //ordenamos el array alfabéticamente
    } else {
      array.sort();
    }
    return array;
  }
}