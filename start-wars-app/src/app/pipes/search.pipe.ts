import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, textFilter?: any): any {
    if(textFilter && textFilter.length > 0) {
      //Filtra los elementos que coinciden en algún campo con el texto buscado
      const filterValue = value.filter(item => {
        return this.compareItemObject(item, textFilter);
      });

      //Si no hay resultados insertamos un elemento de no hay resultados
      if(filterValue.length === 0) {
        filterValue.push({
          id: -1,
          title: 'No hay resultados encontrados'
        })
      }

      return filterValue;
    }
    return [];
  }


  compareItemObject(obj, textFilter) {
    //Si hay algún elemento que coincida con el texto buscado
    return Object.keys(obj).some(key => {
      if (obj[key] != null && typeof obj[key] !== 'undefined') {
        if (typeof obj[key] !== 'object') {
          return String(obj[key]).toLowerCase().indexOf(textFilter.toLowerCase()) !== -1 
        }
        //Si el campo es un array hacemos una llamada recursiva para procesar los campos del array
        if (typeof obj[key] === 'object') {
          this.compareItemObject(obj[key], textFilter);
        }
      }
    });
  }


}
