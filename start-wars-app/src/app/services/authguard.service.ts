import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService  implements CanActivate, CanActivateChild, CanLoad {

  lastStep:number = 0;

  constructor(private router: Router, private api: ApiService) {}

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {

    const url: string = state.url;
    const formattedUrl = url.replace("/",'');
    const result =  this.checkNavigate(url, state);
    return result;
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  canLoad(route: Route): boolean {
    return true;
  }

  checkNavigate(url: string, state: RouterStateSnapshot): boolean {

    let defaultPath = '/register';

    if (url == '/') { 
      return true; 
    }

    //Comprobamos si podemos navegar a la ruta especificada
    const resultState = this.checkState(url, state);

    //Si está permitada, navegamos
    if (resultState.isAllowed) { 
      return true;
    }

    //Si no, navegamos a la ruta actual
    if (resultState.defaultPath) {
      defaultPath  = resultState.defaultPath;
    }

    this.router.navigate([defaultPath])
    return false;
  }

  checkState(url : string, state: RouterStateSnapshot) {
    const currentURI = window.location.hash.replace('#','')
    let isAllowed = true;
    const isAuthenticated = this.api.isAuthenticated();

    //Si el usuario está autenticado, no dejamos a acceder a registro y login
    if(isAuthenticated) {
      if(url.indexOf('register')!== -1 || url.indexOf('login')!== -1) {
        isAllowed = false;
      }
    //Si no está, no dejamos acceder a la página principal
    } else {
      if(url.indexOf('main')!== -1) {
        isAllowed = false;
      }
    }

    return {
      'isAllowed': isAllowed,
      'defaultPath': currentURI
    };
  }
}
