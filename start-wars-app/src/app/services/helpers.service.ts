import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor(private router: Router) { }

  //Función auxiliar de navegación
  navigateTo(nextStep, params?) {
    const navigationExtras: NavigationExtras = {
      queryParamsHandling: 'preserve',
      preserveFragment: true
    };

    if (!params) {
      this.router.navigate([nextStep], navigationExtras)
    } else {
      this.router.navigate([nextStep, params], navigationExtras);
    }    
  }


  //Formatea la hora en formato HH:MM
  formatHour(date: Date) {
    let hours: any = date.getHours();
    let minutes: any = date.getMinutes();
    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    return hours + ":" + minutes;
  }


  //Establece la validación de un campo de un formulario
  updateFieldValidation(form, field, validators = []) {
    form.get(field).setValidators(validators);
    form.get(field).updateValueAndValidity();
  }

  //Actualiza el valor de un campo de un formulario
  updateFieldValue(form, field, value?) {
    if (value === undefined) { value = ''}
    form.get(field).setValue(value);
    form.get(field).updateValueAndValidity();
  }


  //Marca como utilizados todos los campos de un formulario
  updateFormStatus(form) {
    Object.keys(form.controls).forEach(key => {
      form.get(key).markAsTouched();
      form.get(key).markAsDirty();
    });
  }

}
