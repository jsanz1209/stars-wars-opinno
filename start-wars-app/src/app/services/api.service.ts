import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import * as firebase from 'firebase';


const API_URL = environment.apiURL;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private options = {};
  private token = null;

  constructor(
    private httpClient: HttpClient) {
    this.options = { withCredentials: false };
  }


  //REST de registro
  public signinUp(user:string, password: string):Observable<any> {
    const observable =  new Observable(observer => {
      firebase.auth().createUserWithEmailAndPassword(user, password)
      .then(response => {
       observer.next(response);
       observer.complete();
      })
      .catch(error => {
        observer.error(error);
      })      
    });

    return observable;
  }

  //Rest de login
  public signinIn(user:string, password: string):Observable<any> {
    const observable =  new Observable(observer => {
      firebase.auth().signInWithEmailAndPassword(user, password)
      .then(response => {
        firebase.auth().currentUser.getIdToken().then(token => {
          this.token = token;
          sessionStorage.setItem('token', this.token);
        });
       observer.next(response);
       observer.complete();
      })
      .catch(error => {
        observer.error(error);
      })      
    });

    return observable;
  }

  //Comprueba si el usuario está autenticado
  public isAuthenticated() {
    this.token = sessionStorage.getItem('token');
    return this.token !== null;
  }

  //Rest de logout
  public logout():Observable<any>  {
    const observable =  new Observable(observer => {
      firebase.auth().signOut().then(() => {
        this.token = null;
        sessionStorage.removeItem('token');
        observer.next();
        observer.complete();
      })
      .catch(error => {
        observer.error(error);
      });
    
    });
    return observable;
  }

  //Rest de obtenemos películas
  public getFilms(): Observable<any> {
    return this.httpClient
      .get(API_URL + 'films?format=json',
      this.options);
  }

  //Rest de personajes
  public getPeople(): Observable<any> {
    return this.httpClient
      .get(API_URL + 'people?format=json',
      this.options);
  }

  //Rest de información de personaje
  public getPerson(id:string): Observable<any> {
    return this.httpClient
      .get(API_URL + 'people/' + id +  '?format=json',
      this.options);
  }

  //Rest genérico
  public requestGeneric(request) {
    return this.httpClient
    .get(request,
    this.options);
  }
}




