import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ApiService } from './api.service';
import { HISTORIES } from '../collections/history';
import { HelpersService } from './helpers.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationHistoryService {

  history = null;
  //Información con las páginas y descripción que se mostrará en el historial de navegación
  historyInfo = HISTORIES;

  constructor(private router: Router,
              private helpers: HelpersService,
              private api: ApiService) {
    //Recupera de sesión el historial de navegación
    const history = sessionStorage.getItem('history');
    this.history =  history? JSON.parse(history) : null;
  }

  //Añade un página al historial de navegación
  private addPageToHistory(page) {
    const dateCurrent = new Date();
    //Forma un clave que indica la fecha del historial de navegación
    const keyDate = '' + dateCurrent.getDate() + '-' +  dateCurrent.getMonth() + '-' +   dateCurrent.getFullYear();
    //Formatea la hora actual
    const time = this.helpers.formatHour(dateCurrent);

    //Si no hay entradas en el historial
    if(this.history === null) {
      //Añadimos a la fecha el historial
      this.history = {};
      this.history[keyDate] = [
        {
          page: page,
          time: time
        }
      ];
    //En caso contario
    } else {
      //Buscamos si existe esa fecha
      const keyFound = Object.keys(this.history).find(key => {
        return (key === keyDate);
      });

      //Si existe lo añadimos a las entradas de esa fecha
      if(typeof keyFound !== 'undefined') {
        this.history[keyFound].push({
          page: page,
          time: time
        });
      //Si no crea una nueva fecha con la nueva entrada
      } else {
        this.history[keyDate] = [
          {
            page: page,
            time: time
        }];
      }
    }
    //Refresca la sessión con el contenido actual
    sessionStorage.setItem('history', JSON.stringify(this.history));
  }


  //Devuelve el historial de navegación
  getHistory() {
    return this.history;
  }

  //Borra todo el historial de navegación
  deleteAllHistory() {
    this.history = null;
    sessionStorage.removeItem('history');
  }


  //Borra una entrada del historial de navegación
  deleteHistory(date, index) {
   this.history[date].splice(index, 1);
   sessionStorage.setItem('history', JSON.stringify(this.history));
  }

  //Función que se subscribe a los cambios de ruta para añadir un historial de navegación
  public subscribeToNav() {

    this.router.events.subscribe(event => {
      //Si salta el el evento de navegación realizada correctametne y no estamos navegando a la página de historial
      if (event instanceof NavigationEnd && !event.url.includes('history-navigation')) {

        let url = '';
        let description = '';

        //Si es un URL base comprobamos si estamos o no autenticados para guardar el historial
        if(event.url == '/') {
          if(this.api.isAuthenticated()) {
            url = 'main';
          } else {
            url = 'register';
          }
        } else {
          url = event.url.replace(/\//g, '');
          url = url.replace(/-/g, '');
        }

        //Si la navegación es el detalle de un personaje
        if(url.indexOf(this.historyInfo[3].nameHistory) !== -1) {
          const personId = url.substr(url.length - 1, 1);
          description = `${this.historyInfo[3].descriptionHistory} ${personId}`;
        
        //En cualquier otro caso
        } else {
            const historyInfo = this.historyInfo.filter(element => {
                return (element.nameHistory === url);
            });
            if(historyInfo.length > 0) {
              description = historyInfo[0].descriptionHistory;
            }
        }

        //Añadimos la descripción del objeto mapeado
        this.addPageToHistory(description);
      }
    });
  }


}
