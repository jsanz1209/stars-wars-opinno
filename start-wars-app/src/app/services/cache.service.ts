import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  //Cachea los personajes
  private people = null;
  //Cache las películas
  private films = null;

  constructor() { }

  set cacheFilms(data) {
    this.films = data;
  }

  get cacheFilms() {
    if(this.films !== null) {
      return [ ...this.films ];
    }
    return null;
  }

  set cachePeople(data) {
    this.people = data;
  }

  get cachePeople() {
    if(this.people !== null) {
      return [ ...this.people ];
    }
    return null;  
  }
}
