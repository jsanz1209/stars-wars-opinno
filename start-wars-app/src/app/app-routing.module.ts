import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './modules/page-not-found/page-not-found.component';

const appRoutes: Routes = [

  {
    path: '',
    loadChildren: "./modules/register/register.module#RegisterModule"
  },

  {
    path: '',
    loadChildren: "./modules/login/login.module#LoginModule"
  },
  {
    path: '',
    loadChildren: "./modules/main/main.module#MainModule"
  },
  {
    path: '404',
    component: PageNotFoundComponent
  },
  { 
    path: '**', 
    redirectTo: '404' 
  }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false, // <-- debugging purposes only
        useHash: true
      }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: []
})


export class AppRoutingModule { }
