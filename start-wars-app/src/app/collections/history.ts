import { History } from "../models/history.model";

export var HISTORIES: History[] = [
    { 
        nameHistory: 'login',
        descriptionHistory: 'Página de Login'
    },
    { 
        nameHistory: 'register',
        descriptionHistory: 'Página de registro'
    },
    {
        nameHistory: 'main',
        descriptionHistory: 'Página principal con buscador de películas y carousel de personajes'
    },
    {
        nameHistory: 'mainpersondetail',
        descriptionHistory: 'Página de detalle de personaje'
    }
];
  